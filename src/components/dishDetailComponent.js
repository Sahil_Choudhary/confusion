import React,{ Component } from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem, Modal, ModalBody, ModalHeader, Button, Label, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { Loading } from './LoadingComponent';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !val || (val.length<=len);
const minLength = (len) => (val) => val && (val.length>=len);

function RenderDishdetail({ dish }){
    return (
        <div className="col-12 col-md-5 m-1">
            <Card>
                <CardImg width="100%" src={dish.image} alt={dish.name} />
                <CardBody>
                    <CardTitle heading style={{fontWeight:'bold'}}>{dish.name}</CardTitle>
                    <CardText>{dish.description}</CardText>
                </CardBody>
            </Card>
        </div>
    )
}

function RenderComments({ comment }){
    return (
        <div key = {comment.id}>
                <CardText>{comment.comment}</CardText>
                <CardText> -- {comment.author}</CardText>
        </div>

    )
}


class CommentForm extends Component{

    constructor(props) {
        super(props);

        this.toggleModal = this.toggleModal.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
          isModalOpen: false
        };
    }

    toggleModal(){
        this.setState({
            isModalOpen: !this.state.isModalOpen
        });
    }

    handleSubmit(values){
        // alert('Current State is: ' + JSON.stringify(values));
        this.toggleModal()
        this.props.addComment(this.props.dishId, values.name, values.rating, values.comment)
    }
   

    render() {
        return (
            <>
                <Button outline color="secondary" onClick={this.toggleModal}><i class="fa fa-pencil" aria-hidden="true"></i> Submit Comment  </Button>

                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal} >
                    <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                    <ModalBody className='ml-3 mr-3'>
                        <LocalForm onSubmit={(values) => this.handleSubmit(values)}>
                            <Row className='form-group'>
                                <Label htmlFor="Rating">Rating</Label>
                                <Control.select model='.rating' name='rating'
                                    className='form-control custom-select'>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </Control.select>
                            </Row>
                            <Row className='form-group'>
                                <Label htmlFor='name'>Your Name</Label>
                                <Control.text model=".name" id="name" name="name"
                                    placeholder="Your Name"
                                    className="form-control"
                                    validators={{
                                        required,
                                        minLength:minLength(3),
                                        maxLength:maxLength(15)
                                    }}
                                />
                                <Errors
                                    className="text-danger"
                                    model=".name"
                                    show="touched"
                                    messages={{
                                        required: 'Required',
                                        minLength: 'Must be greater than 2 characters',
                                        maxLength:'Must be 15 characters or less'
                                    }} 
                                />
                            </Row>
                            <Row className='form-group'>
                                <Label htmlFor="feedback">Comment</Label>
                                <Control.textarea model=".comment" id="comment" name="comment"
                                    rows='6'
                                    className='form-control'
                                />
                            </Row>
                            <Row className='form-group'>
                                <Button type='submit' color='primary'>Submit</Button>
                            </Row>

                        </LocalForm>
                    </ModalBody>
                </Modal>
                
            </>
        )
    }
}


const DishDetail = (props) => {

        const comments = props.comments.map((comment) => {
            return (
                <RenderComments comment={comment}
                />
            )
        });


        if(props.isLoading){
            return(
                <div className='contsiner'>
                    <div className='row'>
                        <Loading />
                    </div>
                </div>
            )
        }
        else if(props.errMess){
            return(
                <div className='contsiner'>
                    <div className='row'>
                        <h4>{props.errMess}</h4>
                    </div>
                </div>
            )
        }
        else if(props.dish != null){
            return (
                <div className="container">
                    <div className='row'>
                        <Breadcrumb>
                            <BreadcrumbItem>
                                <Link to="/menu">Menu</Link>
                            </BreadcrumbItem>
                            <BreadcrumbItem active>
                                {props.dish.name}
                            </BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>{props.dish.name}</h3>
                            <hr />
                        </div>
                    </div>
                    <div className="row">
                        <RenderDishdetail dish={props.dish}/>
                        <div className="col-12 col-md-5 m-1">
                            <CardTitle style={{fontWeight:'bold'}}>Comments</CardTitle>
                            {comments}
                            <CommentForm  addComment = {props.addComment}
                                dishId = {props.dish.id}/>
                        </div>
                    </div>
                    
                </div>
            )
        }
}


export default DishDetail;